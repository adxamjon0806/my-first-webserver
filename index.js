import express from "express";
import mongoose from "mongoose";
import router from "./router.js";
import fileUpload from "express-fileupload";
//  "mongodb+srv://adxamjon0806:kLH6ETkw6zwKRkbE@cluster0.tj6qahl.mongodb.net/?retryWrites=true&w=majority"

const PORT = process.env.PORT || 5000;
const DB_URL =
  process.env.Mongo_Connect_Url ||
  "mongodb+srv://adxamjon0806:VB0msb2pQlzwTvcV@mymongodb.jwiuk5b.mongodb.net/?retryWrites=true&w=majority";

const app = express();

app.use(express.json());
app.use(express.static("static"));
app.use(fileUpload({}));
app.use("", router);

const start = async () => {
  try {
    await mongoose.connect(DB_URL);
    app.listen(PORT, () => {
      console.log(PORT);
      console.log(DB_URL);
    });
  } catch (e) {
    console.log(e);
  }
};

start();
